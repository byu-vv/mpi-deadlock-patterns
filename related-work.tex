\section{Related Work}\label{related_work}

Much of the literature on predictive program analysis focuses on detecting as many errors as possible from a single observed execution~\cite{DBLP:conf/sigsoft/EslamimehrP14,kalhauge2018sound}.
As mentioned in~\secref{sec:introduction}, the analysis presented here is maximally predictive for single-path programs but cannot reason about messages that are issued in a schedule-dependent manner.
Instead, this paper focuses on improving the efficiency of an SMT based analysis for single-path programs while leaving extensions to more general programs as future work.

The approach in this paper is inspired by several works.
Joshi et al.\ proposed a method for multi-threaded Java programs by first detecting potential lock dependency cycles with a imprecise dynamic analyzer and then finding real deadlocks by a random thread scheduler with high probability~\cite{DBLP:conf/pldi/JoshiPSN09}.
The refining strategy of the work inspires the staged approach taken in this paper, but the tool presented in this paper is guaranteed to report a deadlock if it exists in the single-path program.

Sherlock is a tool that uses concolic execution for deadlock detection in Java programs~\cite{DBLP:conf/sigsoft/EslamimehrP14}.
The key idea is similar to our approach: finding potential deadlocks, and then searching for a feasible schedule that leads to the deadlock.
The difference is that Sherlock repeatedly finds alternate schedules through solving constraints that describe new permutations of previously observed schedules rather than leveraging an abstract machine to filter out false deadlocks.

A precise SMT encoding technique was proposed by Huang et al.\ for verifying properties over MCAPI programs containing message race~\cite{DBLP:conf/kbse/HuangMM13}.
The encoding does not require a precise match set and was extended to checking zero buffer incompatibility for MPI programs~\cite{HuangNFM15}.
%The extended approach provides a set of simple rules for encoding zero buffer semantics. 
This technique is adapted for validating deadlocks in this paper.


The POE approach is a dynamic partial order reduction solution~\cite{DBLP:conf/popl/FlanaganG05} for MPI program verification~\cite{DBLP:conf/ppopp/VakkalankaSGK08,DBLP:conf/sbmf/SharmaGB12}.
%The idea of the work is that the send-receive matches are only determined when each process reaches a blocking call in a program traversal.
%While the solution is able to reduce a set of states for exploration, it still suffers from the scalability problem for large MPI programs.
The approach was extended to POE$_\text{MSE}$, which first uses a precise happens-before relation to find the potential sends that may cause different behaviors based on the initial trace, then replays the execution at each potential send with a different choice, i.e.\ buffering the send instead of matching it~\cite{DBLP:conf/pvm/VakkalankaVGK10}.
%In this way, some other possible schedules are resolved, and deadlocks may be found during the step.
% However, the algorithm is not able to find all the deadlocks.
% Also, the replay is time consuming.

MOPPER is an MPI deadlock detector based on boolean satisfiability encoding~\cite{DBLP:journals/toplas/ForejtJKNS17,DBLP:conf/fm/ForejtKNS14}.
While the solution is precise, the size of the encoding is cubic meaning it only scales to a low degree of message non-determinism.

CIVL is a model checker that uses symbolic execution to verify a number of safety properties of various types of concurrent programs including message passing programs~\cite{siegelDGLRTZZ:2014:civl-tr,DBLP:conf/kbse/ZhengRLDS15,DBLP:conf/sc/SiegelZLZMEDR15}.
The tool is outperformed by MOPPER\@.

An extension to the model checker SPIN~\cite{DBLP:journals/tse/Holzmann97}, is MPI-SPIN that is specific to verifying MPI programs~\cite{DBLP:conf/vmcai/Siegel07,DBLP:conf/pvm/Siegel07}.
%The work is able to check a number of properties by enumerating all the non-deterministic choices for the model it generates.
Since a massive number of states are explored, the work is not scalable.

B{\"{o}}hm et al.\ provide an approach that aims to find deadlocks for an MPI program under both environments of synchronization and no synchronization~\cite{DBLP:conf/fm/BohmMJ16}.
The approach first uses standard partial order reduction to find deadlocks assuming the environment has no synchronization.
It then uses an algorithm to search missed deadlocks by enforcing synchronization in the basic operations such as send and collective operations.

MPI-Checker is a static analyzer based on abstract syntax tree of the source code of MPI programs~\cite{DBLP:conf/sc/DrosteKL15}.
The tool is able to check many errors in a program, However, it is limited to check deadlocks caused by complicated semantics of communication.

ParTypes is a type-based approach for verifying MPI programs by developing a protocol language for a type system~\cite{DBLP:conf/oopsla/LopezMMNSVY15}.
Since the approach is able to avoid traversing the state space, the analysis is scalable for large programs.

Umpire is an approach of runtime verification for checking multiple MPI errors such as deadlock and resource tracking~\cite{DBLP:conf/sc/VetterS00}.
The error checking is taken by spawning one manager thread and several outfielder threads in the execution of an MPI program.
%A drawback of the approach is that it relies on a concrete execution, which may miss the errors in the other execution trace.
%The tool fails for two reasons.
%First, the deadlock detection only considers dependency cycles, therefore, is not able to detect deadlock for orphaned receive pattern.
%Second, it does not scale well since the asynchronous trace transfer highly depends on the thread interleavings which are exponential.
An extension to Umpire is Marmot~\cite{DBLP:conf/parco/KrammerBMR03}.
The work uses a centralized server instead of multiple threads for error checking.
Another extension to Umpire is MUST~\cite{DBLP:conf/ptw/HilbrichSSM09,DBLP:conf/sc/HilbrichPSSM12}.
The structure of MUST allows the users to execute the error checking either in an application process itself or in extra processes that are used to offload these analyses.
%Therefore, MUST scales to programs with more than 1,000 processes.
However, just like Umpire and Marmot, the approach is neither sound nor complete for deadlock detection.

