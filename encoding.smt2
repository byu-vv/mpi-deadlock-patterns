; This is an example of how to implement the encoding used in this paper with an example problem instance
; It is likely less efficient than the implementation we use for benchmarking due to the excessive use of `forall` quantifiers
; However, it is useful for reference since the rules are all laid out in one place
; The basic rules encode the conditions of a feasible execution
; The problem instance encodes a deadlock at a candidate control point
; The 'input' is
; - an Operation const for each operation in the CTP
; - assertions that assign statically known properties to the operations (proc, rank etc.)
; - assertions for the matches that can occur in a deadlocking execution
; You can run this example with `z3 -smt2 encoding.smt2`

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                 Definitions                                  ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; operations
(declare-sort Operation 0)
(declare-datatype OperationType (Send Recv Wait Barr))

; properties bound by the problem instance
(declare-fun typ (Operation) OperationType)
(declare-fun src (Operation) Int)
(declare-fun dst (Operation) Int)
(declare-fun proc (Operation) Int)
(declare-fun rank (Operation) Int)
(declare-fun wait (Operation) Operation)
(declare-fun group (Operation) Int)
(declare-fun complete (Operation) Bool)

; properties solved for
(declare-fun own-time (Operation) Int)
(declare-fun match-time (Operation) Int)

; type discrimination
(define-fun is-send ((op Operation)) Bool (= Send (typ op)))
(define-fun is-recv ((op Operation)) Bool (= Recv (typ op)))
(define-fun is-wait ((op Operation)) Bool (= Wait (typ op)))
(define-fun is-barr ((op Operation)) Bool (= Barr (typ op)))

; happens-before order
(define-fun hb ((a Operation) (b Operation)) Bool
            (and (< (own-time a) (own-time b))
                 (=> (complete b) (complete a))))

; program order
(define-fun po ((a Operation) (b Operation)) Bool
            (and (= (proc a) (proc b))
                 (< (rank a) (rank b))))

; fifo order
(define-fun fifo ((a Operation) (b Operation)) Bool
            (or (and (is-send a)
                     (is-send b)
                     (po a b)
                     (= (dst a) (dst b)))
                (and (is-recv a)
                     (is-recv b)
                     (po a b)
                     (or (= (src a) -1)
                         (= (src a) (src b))))))

; meaning of match 
(define-fun matches ((r Operation) (s Operation)) Bool
            (and (hb s (wait r))
                 (hb r (wait s))
                 (= (match-time r) (own-time s))
                 (= (match-time s) (own-time r))
                 (complete r)
                 (complete s)))

; TODO possibly constrain matching send and recv to have adjacent own-times (strict alternation)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                 Basic Rules                                  ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; unique timestamps
(assert (forall ((a Operation) (b Operation))
                (=> (= (own-time a) (own-time b))
                    (= a b))))

; unique matches
(assert (forall ((a Operation) (b Operation))
                (=> (= (match-time a) (match-time b))
                    (= a b))))

; sends and recvs complete before their enclosing waits
(assert (forall ((op Operation))
                (=> (or (is-recv op) (is-send op))
                    (hb op (wait op)))))

; sends and recvs on a common endpoint complete in fifo order
(assert (forall ((a Operation) (b Operation))
                (=> (fifo a b)
                    (hb a b))))

; non-overtaking sends and recvs
; note that this rule is redundant if matching sends and receives are forced to be adjacent
(assert (forall ((a Operation) (b Operation))
                (=> (fifo a b)
                    (< (match-time a) (match-time b)))))

; waits block
(assert (forall ((a Operation) (b Operation))
                (=> (and (po a b)
                         (or (is-wait a)
                             (is-barr a)))
                    (hb a b))))

; barriers block as a group
(assert (forall ((a Operation) (b Operation) (c Operation))
                (=> (and (is-barr a)
                         (is-barr b)
                         (= (group a) (group b))
                         (po b c))
                    (hb a c))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                              Problem Instance                                ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; constraints for an operation
(define-fun mkop ((op Operation) (ty OperationType) (pr Int) (rk Int) (arg Int)) Bool
            (and (= ty (typ op))
                 (= pr (proc op))
                 (= rk (rank op))
                 (match ty
                        ((Send (and (= pr (src op))
                                   (= arg (dst op))))
                        (Recv (and (= pr (dst op))
                                   (= arg (src op))))
                        (Wait true)
                        (Barr (= arg (group op)))))))

; proc 0
(declare-const s00 Operation)
(assert (mkop s00 Send 0 0 1))
(declare-const w01 Operation)
(assert (mkop w01 Wait 0 1 -1))
(assert (= w01 (wait s00)))
(declare-const b02 Operation)
(assert (mkop b02 Barr 0 2 0))

; proc 1
(declare-const r10 Operation)
(assert (mkop r10 Recv 1 0 -1))
(declare-const w11 Operation)
(assert (mkop w11 Wait 1 1 -1))
(declare-const r12 Operation)
(assert (mkop r12 Recv 1 2 0))
(declare-const w13 Operation)
(assert (mkop w13 Wait 1 3 -1))
(declare-const b14 Operation)
(assert (mkop b14 Barr 1 4 0))

; proc 2
(declare-const s20 Operation)
(assert (mkop s20 Send 2 0 1))
(declare-const w21 Operation)
(assert (mkop w21 Wait 2 1 -1))
(assert (= w21 (wait s20)))
(declare-const b22 Operation)
(assert (mkop b22 Barr 2 2 0))

; actions consumed by abstract machine
; A = {s00, w01, b02,
;      r10, w11, r12, w13,
;      s20, w21}

; deadlock candidate
; D = {b02, w13, w21}

; match-pairs
; M = [ r10 => {s00, s20}
;       r12 => {s00}
;       s00 => {r10, r12}
;       s20 => {r10}]

; recvs find matches not participating in deadlock
; (r, s) for r in prog for s in M(r) if (wait r) not in D and (wait s) not in D
(assert (=> (complete r10) (or (matches r10 s00))))

; sends find matches not participating in the deadlock
; (r, s) for s in prog for r in M(s) if (wait r) not in D and (wait s) not in D
(assert (=> (complete s00) (or (matches r10 s00))))

; blocking operations in deadlock processes that come before the candidate actions must complete
(assert (complete w01))
(assert (complete w11))

; potential matches for deadlocked messages must complete (important for infinite buffer setting where sends don't have waits)
(assert (complete s00))
(assert (complete r10))

(check-sat)
;(get-model)

