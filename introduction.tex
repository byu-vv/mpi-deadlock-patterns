
\section{Introduction}\label{sec:introduction}

The \emph{message passing interface} (MPI) is the \emph{de facto} standard for communication and synchronization in high performance distributed programs.
MPI programs contain a finite set of processes that send and receive messages concurrently.
A common error in MPI programs, referred to as \emph{deadlock}, occurs when one or more processes block indefinitely due to a circular communication dependency.

This paper presents an analysis for detecting deadlock in a subclass of MPI programs called \emph{single-path programs}~\cite{DBLP:journals/toplas/ForejtJKNS17} where the order of actions issued by each process is deterministic for a given input.
Single-path programs therefore exhibit a limited, though still significant, set of non-deterministic behaviors due to \emph{message race}.

Message race occurs when two or more messages are sent concurrently to the same process that has not specified a source for its next receive.
An MPI program that terminates successfully in one execution may deadlock in another execution depending on how message race is resolved by the runtime.

The analysis presented in this paper is \emph{predictive} because it observes a single schedule over message race and finds a different feasible schedule that leads to a deadlock on the same input.
The new schedule preserves the order of actions in each process and is therefore guaranteed to be a feasible execution of the single-path program.
Finding such a schedule is an NP-Complete problem and can be encoded as a propositional formula~\cite{DBLP:journals/toplas/ForejtJKNS17}.

These formulas can grow prohibitively large for many programs because they must encode what it means to deadlock generally.
In contrast, the approach presented in this paper only encodes what it means to deadlock at a specific point in the program.
This simpler encoding is used on a filtered set of likely \emph{deadlock candidates}.

The analysis proceeds in three increasingly precise stages:

\begin{enumerate}
  \item A conservative set of deadlock candidates is extracted from the cycles of a dependency graph defined over the observed execution.
  \item Each candidate is tested for reachability in an abstract machine that implements an abstract semantics for the presented MPI programming model.
  \item The feasibility of the remaining candidates is determined by encoding each as a separate SMT problem.
\end{enumerate}

Each of these stages presents its own research challenges.
First, highly non-deterministic MPI programs yield large dependency graphs with a huge number of cycles.
A naive enumeration of all cycles would not scale to these programs.
This paper presents a strict characterization of which cycles may be deadlock candidates and a modification of Johnson's algorithm~\cite{DBLP:journals/siamcomp/Johnson75} that only enumerates such cycles.

Second, to be an effective filtering mechanism, the abstract machine must be able to accept all feasible deadlock cycles and reject most infeasible deadlock cycles based on only one abstract execution.
In other words, the machine must be provably precise and the schedule chosen by the abstract machine execution must not lead to spurious results.
Proofs for both of these properties are given in the full paper \cite{paper-long}.

Finally, the SMT encoding is adapted from an encoding developed by Huang et al~\cite{HuangNFM15} that can only describe schedules over message race that form complete executions (e.g., the end of each process is reached).
The \emph{witness execution}, which is an execution that actually does deadlock at the predicted program location, for a feasible deadlock candidate is necessarily incomplete.
Therefore, the encoding is adapted to describe partial executions that deadlock at a specific point.
This is achieved by adding one additional boolean variable for every encoded MPI operation that represents its completion state in the witness execution.
Operations in the processes named by the deadlock candidate are asserted as complete up to the deadlock point while the completion state of operations in the other processes is resolved by the SMT solver as needed to reach the deadlock.


The analysis supports multiple buffer settings and is sound and complete for single-path programs on a given input (i.e., it reports a deadlock if and only if there is a reachable deadlock).
Empirical results show that, in many cases, the staged analysis is far more efficient than encoding the deadlock problem as an SMT formula directly~\cite{DBLP:conf/fm/ForejtKNS14,DBLP:journals/toplas/ForejtJKNS17} or analyzing the program with a model checker~\cite{DBLP:conf/ppopp/VakkalankaSGK08,DBLP:conf/sbmf/SharmaGB12,DBLP:conf/fm/BohmMJ16}.

