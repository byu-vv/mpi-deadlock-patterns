package Syntax;

public class NoComm extends Operation {
	
	
	
	public NoComm(String event, Process process) {
		super(event, process);
	}
	
	public String toString()
	{
		return "NoComm" + event;
	}
}

