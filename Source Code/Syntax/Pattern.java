package Syntax;

import java.util.HashSet;
import java.util.Hashtable;

public class Pattern {
	public Program program;
	public Hashtable<Integer, Operation> op_map;
	
	public static Pattern generatePattern(Hashtable<Integer, Operation> patterns,Program program)
	{
		Pattern newPattern = new Pattern();
		//reference the pattern source
		newPattern.op_map= patterns;
		return newPattern; 
	}
	
	public HashSet<Integer> getRecvDest()
	{
		HashSet<Integer> dest = new HashSet<Integer>();
		for(Operation op : op_map.values())
		{
			if(op instanceof Recv)
			{
				dest.add(op.process.rank);
			}
		}
		return dest;
	}
	
	public HashSet<Integer> getRecvSrc()
	{
		HashSet<Integer> src = new HashSet<Integer>();
		for(Operation op : op_map.values())
		{
			if(op instanceof Recv)
			{
				Recv r = (Recv)op;
				if(r.src != -1)
					src.add(r.src);
				else
				{
					for(Process p : program.processes)
						src.add(p.rank);
					return src;
				}
			}
		}
		return src;
	}
}