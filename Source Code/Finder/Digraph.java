package Finder;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Set;
import java.util.Vector;

import Syntax.*;
import Syntax.Process;

public class Digraph 
{
	public Vector<Operation> Vlist;
//	public Hashtable<Operation, Set<E>> Etable;
	public Hashtable<Operation, Set<Operation>> Etable;
	public Program program;
//	public Hashtable<Recv, LinkedList<Send>> matches;
	int Vrank = 0;
	public int type=0; //0--infinite buffer, 1--zero buffer, 2--combined pattern
	
	public Digraph(Program p) throws Exception
	{
		program = p;
		Vlist = new Vector<Operation>();
		Etable = new Hashtable<Operation, Set<Operation>>();
//		matches = p.match_table;
		
		initGraph();
	}
	
	
	public Digraph(Program p,int tp) throws Exception
	{
		program = p;
		Vlist = new Vector<Operation>();
		Etable = new Hashtable<Operation, Set<Operation>>();
//		matches = p.match_table;
		type = tp;
		initGraph();
		
		System.out.println(Vlist);
		System.out.println(Etable);
	}
	
	public void initGraph() throws Exception
	{
		if(type == 0)
			generateGraph();
		else if(type == 1) generateGraph_Zero();
		else if(type == 2) generateGraph_all();
		else if(type == 3) generateGraph_all_Imp();
	} 
	
	
	
	public Set<Operation> adj(Operation v)
	{
		if(!Etable.containsKey(v))
			return null;
		return Etable.get(v);
 	}
	
	public int getVSize()
	{
		return Vlist.size();
	}
	
	//TODO: should consider the nearest-enclosing wait in graph generation
	public void generateGraph() throws Exception
	{
		for(Process process: program.processes)
		{
			//add all vertices and partial hb relations from each process
			process.generateVE();
			Vlist.addAll(process.vertices);
			Etable.putAll(process.HB);
		}
		
		program.generateMatch();
		//program.displayMatch();
		//add match relation as edge for vertices
		Hashtable<Recv, LinkedList<Send>> match_table = program.match_table;
		continuepoint:
		for(Operation op : Vlist)
		{
			if(!(op instanceof Recv))
			{
				continue continuepoint;
			}
			
			Recv r = (Recv)op;
			if(!match_table.containsKey(r))
			{
				continue continuepoint;
			}
			
			continuepoint1:
			for(Send s : match_table.get(r))
			{
				if(!Vlist.contains(s))
				{
					continue continuepoint1;
				}
				
				if(!Etable.containsKey(s))
					Etable.put(s, new HashSet<Operation>());
				Etable.get(s).add(r);
			}
		}
		
		//System.out.println(Vlist+"\n"+Etable);
	}
	
	public void generateGraph_Zero() throws Exception
	{
		for(Process process: program.processes)
		{
			//add all vertices and partial hb relations from each process
			process.generateVE_Zero();
			Vlist.addAll(process.vertices);
			Etable.putAll(process.HB);
		}
		
		//add both match relation and reversed match relation as edges for vertices
		program.generateMatch();
		//program.displayMatch();
		Hashtable<Recv, LinkedList<Send>> match_table = program.match_table;
		continuepoint:
		for(Operation op : Vlist)
		{
			if(!(op instanceof Recv))
			{
				continue continuepoint;
			}
			
			Recv r = (Recv)op;
			if(!match_table.containsKey(r))
			{
				continue continuepoint;
			}
			
			if(!Etable.containsKey(r))
				Etable.put(r, new HashSet<Operation>());
			
			continuepoint1:
			for(Send s : match_table.get(r))
			{
				if(!Vlist.contains(s))
				{
					continue continuepoint1;
				}
				Etable.get(r).add(s);
				/*if(!Etable.containsKey(s)) //add reversed match relation
					Etable.put(s, new HashSet<Operation>());
				Etable.get(s).add(r);*/
			}
		}
		
		//System.out.println("vlist: " + Vlist);
		//System.out.println("Etable: " + Etable);
	}
	
	public void generateGraph_all() throws Exception
	{
		
		//generate nocomm nodes for all processes
		NoComm[] nocomm = new NoComm[program.processes.size()];
		for(Process process:program.processes)
			nocomm[program.processes.indexOf(process)] 
					= new NoComm(process.getRank()+"_"+process.ops.size(), process);
		
		
		for(Process process: program.processes)
		{
			//add all vertices and partial hb relations from each process
			process.generateVE_Zero();
			Vlist.addAll(process.vertices);
			Etable.putAll(process.HB);
			
			for(Operation op : process.vertices)//rule 5
			{
				if(!Etable.containsKey(op))
					Etable.put(op, new HashSet<Operation>());
				Etable.get(op).add(nocomm[program.processes.indexOf(process)]);
			}
			Vlist.add(nocomm[program.processes.indexOf(process)]);
			
			//rule 3
			for(Operation op1 : process.HB.keySet())
			{
				for(Operation op2: process.HB.get(op1))
				{
					if(op1 instanceof Recv && op2 instanceof Recv && ((Recv)op1).src==-1 && ((Recv)op2).src != -1) //rule 3
					{
						int src = ((Recv)op2).src;
						if(!Etable.containsKey(nocomm[src]))
							Etable.put(nocomm[src], new HashSet<Operation>());
						Etable.get(nocomm[src]).add(op2);
					}
				}
			}
			
			//rule4
			for(Operation op1 : process.ops)
			{
				if(op1 instanceof Send)
				{
					int dest = ((Send)op1).dest;
					for(Operation op2 : program.processes.get(dest).ops)
					{
						if(op2 instanceof Recv && ((Recv)op2).src == -1)
						{
							if(!Etable.containsKey(nocomm[dest]))
								Etable.put(nocomm[dest], new HashSet<Operation>());
							Etable.get(nocomm[dest]).add(op2);
						}
					}
				}
			}
			
		}
		
		//add both match relation and reversed match relation as edges for vertices
		program.generateMatch();
		//program.displayMatch();
		Hashtable<Recv, LinkedList<Send>> match_table = program.match_table;
		continuepoint:
		for(Operation op : Vlist)
		{
			if(!(op instanceof Recv))
			{
				continue continuepoint;
			}
			
			Recv r = (Recv)op;
			if(!match_table.containsKey(r))
			{
				continue continuepoint;
			}
			
			if(!Etable.containsKey(r))
				Etable.put(r, new HashSet<Operation>());
			
			continuepoint1:
			for(Send s : match_table.get(r))
			{
				if(!Vlist.contains(s))
				{
					continue continuepoint1;
				}
				Etable.get(r).add(s);
				/*if(!Etable.containsKey(s)) //add reversed match relation
					Etable.put(s, new HashSet<Operation>());
				Etable.get(s).add(r);*/
			}
		}
		
		//System.out.println("vlist: " + Vlist);
		//System.out.println("Etable: " + Etable);
	}
	
	public void generateGraph_all_Imp() throws Exception
	{
		
		//generate nocomm nodes for all processes
		NoComm[] nocomm = new NoComm[program.processes.size()];
		for(Process process:program.processes)
			nocomm[program.processes.indexOf(process)] 
					= new NoComm(process.getRank()+"_"+process.ops.size(), process);
		
		
		for(Process process: program.processes)
		{
			//add all vertices and partial hb relations from each process
			process.generateVE_Zero();
			Vlist.addAll(process.vertices);
			Etable.putAll(process.HB);
			
			for(Operation op : process.vertices)//rule 5
			{
				if(!Etable.containsKey(op))
					Etable.put(op, new HashSet<Operation>());
				Etable.get(op).add(nocomm[program.processes.indexOf(process)]);
			}
			Vlist.add(nocomm[program.processes.indexOf(process)]);
			
			//rule 3
			for(int i = 0; i < process.ops.size(); i++)
			{
				Operation op1 = process.ops.get(i);
				if(op1 instanceof Recv &&((Recv)op1).src==-1)
				{
					for(int j = i+1; j < process.ops.size(); j++)
					{
						Operation op2 = process.ops.get(j);
						if( op2 instanceof Recv && ((Recv)op2).src != -1) //rule 3
						{
							int src = ((Recv)op2).src; //the source for the deterministic receive a
							int count_snd = 0;
							int count_preR = 0; //
							for(Operation snd : program.processes.get(src).ops) // the number of all sends in src(a)
							{
								if(snd instanceof Send)
									count_snd ++;
							}
							for(Operation op : process.ops)// the number of receives in src(a) such that each receive <po a
							{
								if(op instanceof Recv)
								{
									if(op.equals(op2))
										break;
									else
										count_preR ++;
								}
							}
							if(count_snd <= count_preR){
								if(!Etable.containsKey(nocomm[src]))
									Etable.put(nocomm[src], new HashSet<Operation>());
								Etable.get(nocomm[src]).add(op2);
							}
						}
					}
				}
			}
			
			//rule4
			int count_preS = 0;
			for(Operation op1 : process.ops)
			{
				if(op1 instanceof Send)
				{
					int dest = ((Send)op1).dest;
					int src = ((Send)op1).src;
					int count_matchingR = 0;
					for(Operation rcv : program.processes.get(dest).ops)//the number of receives that potentially match op1
					{
						if(rcv instanceof Recv && (((Recv)rcv).src == -1 || ((Recv)rcv).src == src))
							count_matchingR ++;
					}
					if(count_preS >= count_matchingR)
					{
						for(Operation op2 : program.processes.get(dest).ops)
						{
							if(op2 instanceof Recv && ((Recv)op2).src == -1)
							{
								if(!Etable.containsKey(nocomm[dest]))
									Etable.put(nocomm[dest], new HashSet<Operation>());
								Etable.get(nocomm[dest]).add(op2);
							}
						}
					}
					count_preS ++;
				}
			}
			
		}
		
		//add both match relation and reversed match relation as edges for vertices
		program.generateMatch();
		//program.displayMatch();
		Hashtable<Recv, LinkedList<Send>> match_table = program.match_table;
		continuepoint:
		for(Operation op : Vlist)
		{
			if(!(op instanceof Recv))
			{
				continue continuepoint;
			}
			
			Recv r = (Recv)op;
			if(!match_table.containsKey(r))
			{
				continue continuepoint;
			}
			
			if(!Etable.containsKey(r))
				Etable.put(r, new HashSet<Operation>());
			
			continuepoint1:
			for(Send s : match_table.get(r))
			{
				if(!Vlist.contains(s))
				{
					continue continuepoint1;
				}
				Etable.get(r).add(s);
				/*if(!Etable.containsKey(s)) //add reversed match relation
					Etable.put(s, new HashSet<Operation>());
				Etable.get(s).add(r);*/
			}
		}
		
		//System.out.println("vlist: " + Vlist);
		//System.out.println("Etable: " + Etable);
	}
	
//	public void generateV()
//	{
//		
//		BitSet wildcardissued = new BitSet(program.size());
//		BitSet sendissued = new BitSet(program.size());
//		for(Process process: program.processes)
//		{
//			continuepoint:
//			for(int i = 0; i < process.size(); i++)
//			{
//				//now consider all the receives (wildcard, determinstic)
//				//TODO: 1) for any receive r and a sequence of sends with identical src and dest,
//				//only one vertex for r and the first send in this sequence needs to be generated
//				//2) for any deterministic receive r, if a wildcard receive is issued before r, 
//				//r is not considered in any vertex (it is only considered in mismatched endpoint pattern)
//				if(process.get(i) instanceof Recv)
//				{
//					Recv r = (Recv)process.get(i);
//					if(r.src == -1)
//						wildcardissued.set(process.getRank());
//				
//					if(r.src != -1 && wildcardissued.get(process.getRank()))
//					{
//						//this case is only considered in mismatched endpoint pattern
//						continue continuepoint;
//					}
//					
//					sendissued.clear();
//					for(int j = i; j < process.size(); j++)
//					{
//						if(process.get(j) instanceof Send)
//						{
//							Send s = (Send)process.get(j);
//							if(!sendissued.get(s.dest))
//								Vlist.add(V.generateV(r, s));
//							//mark a send from src to dest is issued
//							//so no need to generate another vertex for sends with src and dest
//							sendissued.set(s.dest);
//
//						}
//					}
//				}
//			}
//		}
//	}
	
//	public void generateE()
//	{
//		//use match pairs
//		for(V v: Vlist)
//		{
//			Recv r = v.getRecv();
//			LinkedList<Send> matchedS = matches.get(r);
//			for(Send matcheds : matchedS)
//			{
//				for(V matchedv : Vlist)
//				{
//					if(matchedv.send.equals(matcheds))
//					{
//						if(!Etable.containsKey(matchedv))
//							Etable.put(matchedv, new HashSet<E>());
//						
//						E newE = E.generateE(matchedv, v);
//						Etable.get(matchedv).add(newE);
////						System.out.println(matchedv + " add Edge " + newE);
//					}
//				}
//			}	
//		}
//	}
}
