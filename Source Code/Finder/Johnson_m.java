package Finder;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import Syntax.Operation;
import Syntax.Recv;
import Syntax.Send;

public class Johnson_m 
{    
    public LinkedList<Hashtable<Integer, Operation>> patterns;
    private Stack<Operation> stack;
    Digraph G;
    private int leastvertex;
    
    int count_cut = 0;
   // public boolean isZero = false;
    
    
    public Johnson_m(Digraph Graph)
    {
    	G = Graph;
    	//isZero= isZ;
        stack = new Stack<Operation>();
        patterns = new LinkedList<Hashtable<Integer, Operation>>(); 
        find();
    }
    
    public HashSet<Operation> leastSCC(int lowerbound)
    {
    	TarjanSCC tarjan = new TarjanSCC(G, lowerbound);
    	leastvertex = tarjan.leastvertex;
    	//the result could be null
    	return tarjan.leastSCC;
    }
    
    public void find()
    {
    	Hashtable<Operation, Boolean> blocked = new Hashtable<Operation, Boolean>();
        Hashtable<Operation, List<Operation>> blockedNodes = new Hashtable<Operation, List<Operation>>();
    	stack.empty();
    	int s = 0;
    	while(s < G.Vlist.size()-1)
    	{    		
    		Set<Operation> leastSCC = leastSCC(s);

    		if(leastSCC != null)
    		{
    			s = leastvertex;
    			for(Operation v : leastSCC)
    			{
    				blocked.put(v, false);
                    blockedNodes.put(v, new LinkedList<Operation>());
    			}
                boolean dummy = circuit(leastSCC, s, s, stack, blocked, blockedNodes);
                s++;
    		}
    		else
    		{
    			s = G.Vlist.size()-1;
    		}
    	}
    	
    }
    
    public boolean circuit(Set<Operation> dg, int v, int s, Stack<Operation> stack, 
    		Hashtable<Operation, Boolean> blocked,
    		Hashtable<Operation, List<Operation>> blockedNodes) 
    {
    	//System.out.println("["+v+"]");
    	if (dg == null) { return false; }
        if (dg.size() == 0) { return false; }
        boolean f = false;
        Operation vertex = G.Vlist.get(v);
        Operation startvertex = G.Vlist.get(s);
        stack.push(vertex);
        blocked.put(vertex, true);
      //all the operation in leastSCC that v can connect
        HashSet<Operation> adj_leastSCC = new HashSet<Operation>(); 
        continuepoint:
        for (Operation w : G.Etable.get(vertex)) {
        	//only vertex in leastSCC is considered
        	if(!dg.contains(w))
        		continue continuepoint;
        	adj_leastSCC.add(w);
            if (w == startvertex) {
                stack.push(startvertex);
                
                
                //pattern generation based on the detected circuit
                if(stack.size() > 3) //remove the circle for two operations S and R, where S and R connect with each other.
                {
                    //System.out.println("stack:" + stack.toString());
                	
                	//check if the circle in stack is a real dependency we wanted!
                	Hashtable<Integer, List<Operation>> ProcessOps = new Hashtable<Integer, List<Operation>>();
                	
                	for(Operation op:stack)
                	{
                		if(!ProcessOps.containsKey(op.process.getRank()))
                			ProcessOps.put(op.process.getRank(), new LinkedList<Operation>());
                		if(!ProcessOps.get(op.process.getRank()).contains(op))
                			ProcessOps.get(op.process.getRank()).add(op);
                	}
                	
                	//System.out.println("processops:" + ProcessOps);
                	
                	for(int ProS : ProcessOps.keySet())
                	{
                		//every process in the pattern must have at least two operations
                		if(ProcessOps.get(ProS).size()<=1)
                		{
                			stack.pop();
                			continue continuepoint;
                		}
                		Operation firstOp = ProcessOps.get(ProS).get(0);;                			
                			
                		boolean connect = false;
                		for(int ProD : ProcessOps.keySet())
                		{
                			if(ProD == ProS)
                				continue;
                			Operation lastOp = ProcessOps.get(ProD).get(ProcessOps.get(ProD).size()-1);
                			if(G.Etable.containsKey(firstOp)&&G.Etable.get(firstOp).contains(lastOp)
                					||G.Etable.containsKey(lastOp)&&G.Etable.get(lastOp).contains(firstOp))
                			{
                				connect = true;
                			}
                		}
                		
                		if(!connect)
                		{
                			stack.pop();
                			continue continuepoint;
                		}
                	}
                	
	                Hashtable<Integer, Operation> pattern = new Hashtable<Integer, Operation>();
	                //Hashtable<Integer, Send> pattern_zero=new Hashtable<Integer,Send>();
	               // System.out.println("Stack:" + stack );
	            	//System.out.println("stack:" + stack);
	                for(Operation op : stack)
	                {
	                	count_cut ++;
		                //add the first operation of each process in stack to the pattern
		                //if(!(op instanceof Recv))
		               		//continue;
		               	//Recv r = (Recv)op;
	                	int opEP = op.process.getRank();
	                	int opOrder = op.process.ops.indexOf(op);
	                	
	                		
	                	//System.out.println(op+",EP:"+opEP+",rank:"+opOrder);
	                	
		               	if(!pattern.containsKey(opEP))
		               	{
		               		pattern.put(opEP, op);
		               		continue;
		               	}
		               	//only keep the operation with lowest rank on each process
		               	int order = Integer.valueOf(pattern.get(opEP).process.ops.indexOf(pattern.get(opEP)));
		               					               	
		               	if(order > opOrder)
		               	{
		               		pattern.put(opEP, op);
		               	}
		             
	                }
	                

	                if( pattern.size() > 1 && !patterns.contains(pattern)){
	                	//System.out.println("pattern:"+pattern);
	                	patterns.add(pattern);
	                }
	                
	                if(count_cut>=1000000000)
	                	return f;
                }
                stack.pop();
                f = true;
            }
            else {
                if (! blocked.get(w)) {
                    if (circuit(dg, G.Vlist.indexOf(w), s, stack,blocked,blockedNodes)) { f = true; }
                }
            }
        }
        if (f) { unblock(vertex,blocked,blockedNodes); }
        else {
            for (Operation w : adj_leastSCC) {
                if (! blockedNodes.get(w).contains(vertex)) {
                    blockedNodes.get(w).add(vertex);
                }
            }
        }
        stack.pop();
        return f;
    }
    
    //recursion 
    public void unblock(Operation v, Hashtable<Operation, Boolean> blocked,
    		Hashtable<Operation, List<Operation>> blockedNodes)
    {
    	blocked.put(v, false);
        while (blockedNodes.get(v).size() > 0) {
            Operation w = blockedNodes.get(v).remove(0);
            if (blocked.get(w)) {
                unblock(w,blocked,blockedNodes);
            }
        }
    }
    
    public void printCircles(LinkedList<Hashtable<Integer, Set<Operation>>> circles)
    {
    	for(int i = 0; i < circles.size(); i++)
    	{
    		System.out.print("Circle[" + i + "]:");
    		System.out.println(circles.get(i));
    	}
    }

}
