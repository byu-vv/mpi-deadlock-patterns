package Finder;

import Syntax.*;
import Syntax.Process;

import com.microsoft.z3.*;

public class Test4 {

	public static void main(String[] args) throws Exception {
		ZeroIncompat_Checker finder1;
		Program program = test3();
		program.InitGraph();
	    finder1 = new ZeroIncompat_Checker(program);
		finder1.Run();
		
	}
	
	public static Program test1()
	{
		Program program;
		Process process0,process1;
		//test case 1
		//S(1)    S(0)
		//R(0)    R(1)
		program = new Program(true);
		process0 = new Process(0);
		process1 = new Process(1);
		program.add(process0);
		program.add(process1);
		process0.add(new Send(process0.getRank() + "_" + 0, process0,0, 0, 1, null, 2, 
				true, null));
		process0.add(new Recv(process0.getRank()+ "_" + 1, process0,0, 1, 
				0, null, true, null));
				
		process1.add(new Send(process1.getRank() + "_" + 0, process1,0, 1, 0, null, 2, 
				true, null));
		process1.add(new Recv(process1.getRank()+ "_" + 1, process1,0, 0, 
				1, null, true, null));
		
		return program;
	}
	
	public static Program test2()
	{
		Program program;
		Process process0,process1,process2;
		//test case 1
		//R(1)    S(2)   S(0)
		//R(2)    S(0)   R(1)
		program = new Program(true);
		process0 = new Process(0);
		process1 = new Process(1);
		process2 = new Process(2);
		program.add(process0);
		program.add(process1);
		program.add(process2);
		process0.add(new Recv(process0.getRank()+ "_" + 0, process0,0, 1, 
				0, null, true, null));
		process0.add(new Recv(process0.getRank()+ "_" + 1, process0,0, 2, 
				0, null, true, null));
				
		process1.add(new Send(process1.getRank() + "_" + 0, process1,0, 1, 2, null, 2, 
				true, null));
		process1.add(new Send(process1.getRank() + "_" + 1, process1,0, 1, 0, null, 2, 
				true, null));
		
		process2.add(new Send(process2.getRank() + "_" + 0, process2,0, 2, 0, null, 2, 
				true, null));
		process2.add(new Recv(process2.getRank()+ "_" + 1, process2,0, 1, 
				2, null, true, null));
		
		return program;
	}
	
	public static Program test3()
	{
		Program program;
		Process process0,process1,process2;
		//test case 1
		//S(1)    R(0)
		//R(1)    S(2)   S(0)
		//R(2)    S(0)   R(1)
		program = new Program(true);
		process0 = new Process(0);
		process1 = new Process(1);
		process2 = new Process(2);
		program.add(process0);
		program.add(process1);
		program.add(process2);
		process0.add(new Send(process0.getRank() + "_" + 0, process0,0, 0, 1, null, 2, 
				true, null));
		process0.add(new Recv(process0.getRank()+ "_" + 1, process0,0, 1, 
				0, null, true, null));
		process0.add(new Recv(process0.getRank()+ "_" + 2, process0,0, 2, 
				0, null, true, null));
				
		process1.add(new Recv(process1.getRank()+ "_" + 0, process1,0, 0, 
				1, null, true, null));
		process1.add(new Send(process1.getRank() + "_" + 1, process1,0, 1, 2, null, 2, 
				true, null));
		process1.add(new Send(process1.getRank() + "_" + 2, process1,0, 1, 0, null, 2, 
				true, null));
		
		process2.add(new Send(process2.getRank() + "_" + 0, process2,0, 2, 0, null, 2, 
				true, null));
		process2.add(new Recv(process2.getRank()+ "_" + 1, process2,0, 1, 
				2, null, true, null));
		
		return program;
	}

}
