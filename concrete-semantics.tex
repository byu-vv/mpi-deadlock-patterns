\section{Semantics of Concurrent Trace Programs}\label{sec:concrete-semantics}

\begin{figure}
  \centering
  \issuet\ Transition
  \begin{tabular}{>{\centering\arraybackslash}p{\linewidth}}
    \\[\sep]
    $\act \in \acts \setminus \issued$\ \ \ \ $\preimage{\po}{\act} \subseteq \issued$\ \ \ \ $(\preimage{\po}{\act} \cap (\waits{\acts} \cup \barrs{\acts})) \subseteq \matched$ \\
    \hline
    $\statetuple{\acts}{\issued}{\matched} \concretet \statetuple{\acts}{\issued \cup \{\act\}}{\matched}$
    \\[\sep]\\
  \end{tabular}

  \matchsendrecvt\ Transition
  \begin{tabular}{>{\centering\arraybackslash}p{\linewidth}}
    \\[\sep]
    $\act,\act^\prime \in \issued \setminus \matched$\ \ \ \ $\act \in \sends{\acts}$\ \ \ \ $\act^\prime \in \recvs{\acts}$\ \ \ \ $\actdst{\act} = \actdst{\act^\prime}$\ \ \ \ $\actsrc{\act^\prime} \in \{\actsrc{\act}, *\}$\ \ \ \ $(\preimage{\mo}{\act} \cup \preimage{\mo}{\act^\prime}) \subseteq \matched$ \\
    \hline
    $\statetuple{\acts}{\issued}{\matched} \concretet \statetuple{\acts}{\issued}{\matched \cup \{\act, \act^\prime\}}$
    \\[\sep]\\
  \end{tabular}

  \matchwaitt\ Transition
  \begin{tabular}{>{\centering\arraybackslash}p{\linewidth}}
    \\[\sep]
    $\act \in \issued \setminus \matched$\ \ \ \ $\act \in \waits{\acts}$\ \ \ \ $\preimage{\mo}{\act} \subseteq \matched$ \\
    \hline
    $\statetuple{\acts}{\issued}{\matched} \concretet \statetuple{\acts}{\issued}{\matched \cup \{\act\}}$
    \\[\sep]\\
  \end{tabular}

  \matchbarrt\ Transition
  \begin{tabular}{>{\centering\arraybackslash}p{\linewidth}}
    \\[\sep]
    $\grp \in \groups{\acts}$\ \ \ \ $\forall \act \in \groupacts{\acts}{\grp}, \act \in \issued \wedge \preimage{\mo}{\act} \subseteq \matched$ \\
    \hline
    $\statetuple{\acts}{\issued}{\matched} \concretet \statetuple{\acts}{\issued}{\matched \cup \groupacts{\acts}{\grp}}$
    \\[\sep]\\
  \end{tabular}
  \caption{Transition Rules for Concrete Semantics.}\label{fig:concrete}
\end{figure}

In this section, we extend the semantics presented by Forejt et al.~\cite{DBLP:journals/toplas/ForejtJKNS17} to accomodate barrier groups and simpler proofs of correctness for our analysis.
The semantics of $\acts$ is given by a finite state machine $\machine{\acts} = \machinetuple{\statespace}{\astate_0}{\concretet}$ where $\statespace \subseteq 2^\allacts \times 2^\allacts \times 2^\allacts$ is the set of states, $\astate_0 = \statetuple{\acts}{\emptyset}{\emptyset}$ is the start state and $\concretet \subseteq \statespace \times \statespace$ is the transition relation.
In a state $\astate = \statetuple{\acts}{\issued}{\matched}$, $\acts$ is the set of actions in the CTP, $\issued \subseteq \acts$ is the set of actions that have been issued to the runtime, and $\matched \subseteq \issued$ is the set of actions that have been \emph{matched}.

Send and receive actions are matched together by the runtime when their source and destination processes are compatible.
Wait actions do not match other actions but are instead ``matched'' with themselves after their associated message request is matched.
Finally, barrier actions match the other actions in their group when they have all been issued.

Some actions in the same process may be matched in an order different from how they were issued while others must be matched in the order they were issued.
This constraint is captured by two more partial orders.

\begin{definition}[Queue order]\label{def:queue-order}
  Queue order is a partial order $(\acts, \qoeq^{\acts})$ where for all actions $\act,\act^\prime \in \acts$, $\act \qoeq^{\acts} \act^\prime$ if and only if $\act \poeq^{\acts} \act^\prime$ and one of the following is true:
  \begin{enumerate}
    \item $\{\act, \act^\prime\} \subseteq \sends{\acts} \wedge \actdst{\act} = \actdst{\act^\prime}$
    \item $\{\act, \act^\prime\} \subseteq \recvs{\acts} \wedge \actsrc{\act} \in \{\actsrc{\act^\prime}, *\}$
  \end{enumerate}
\end{definition}

\defref{def:queue-order} defines a first-in-first-out (FIFO) ordering over messages communicated on the same endpoint.
With one exception, this order fully supports the ``non-overtaking'' property of ordered messages as defined in the MPI standard.
The exception occurs when a deterministic receive is followed by a wildcard receive in the same process.

In this case, FIFO ordering over the two receive actions is enforced only if they can both match the same send action.
This condition is schedule-dependent as its value changes depending on whether a send action has been issued that can match the first receive action when the second action is issued.
As in~\cite{DBLP:journals/toplas/ForejtJKNS17}, we leave such receive actions unordered.

\begin{definition}[Match order]\label{def:match-order}
  Match order is a partial order $(\acts, \moeq^{\acts})$ where for all actions $\act,\act^\prime \in \acts$, $\act \moeq^{\acts} \act^\prime$ if and only if $\act \poeq^{\acts} \act^\prime$ and one of the following is true:
  \begin{enumerate}
    \item $\act \qoeq^{\acts} \act^\prime$
    \item $\act \in \waits{\acts} \cup \barrs{\acts}$
    \item $\act \in \sends{\acts} \cup \recvs{\acts} \wedge \act^\prime \in \waits{\acts} \wedge \act = \actreq{\act^\prime}$
  \end{enumerate}
\end{definition}

\defref{def:match-order} ensures that (1) queue order is preserved when messages are matched, (2) blocking actions are matched before subseqent actions in the same process and (3) message requests are matched before their associated wait actions.
With match order defined, we can define the transition relation $\concretet$ shown in~\figref{fig:concrete}.
Given a relation $Q$ over a set $X$ and an element $x \in X$, $\preimage{Q}{x} = \{ y \in X\ :\ y Q x \}$ is the preimage of $x$ under $Q$.
The \issuet\ transition issues a new action when all of the actions preceding it in the same process have been issued and all of the blocking actions preceding it in the same process have been matched.
The \matchsendrecvt\ transitions complete type compatible matches when their match order dependencies have been satisfied.

Note that the definition of the \matchwaitt\ transition in $\concretet$ requires that the \matchsendrecvt\ transition has completed for the message request of the wait action.
This effectively forces executions to follow a \emph{rendevous protocol} where waiting for a communication causes the process to block until the full message transfer is completed.
This is in contrast to an equally valid protocol where wait actions are matched as soon as the data buffer used in the message request is available.

We choose to use the rendevous protocol because it is equivalent to a zero-buffer runtime.
In a buffered runtime, sent messages can be copied into system buffers before matching receive actions are issued in order to free up user buffers more quickly.
In a zero-buffer runtime, this buffering never occurs and the user buffer is only available after the message transfer has completed.

Our analysis can also support another important buffer setting for MPI programs: the infinite-buffer setting.
In an infinite-buffer runtime, sent messages are buffered without limit.
The \matchwaitt\ transition is incompatible with this buffer setting.
Rather than creating new transition rules however, we completely support the infinite-buffer setting by removing wait actions from the CTP that wait on send actions.

