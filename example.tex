
\section{Example}\label{sec:example}

\begin{figure}
  \centering
  \begin{tabular}{l|l|l}
                        & $\recv{0}{1}{*}$   &                   \\
                        &                    & $\send{1}{2}{1}$  \\
                        & $\wait{2}{1}{0}$   &                   \\
    $\send{3}{0}{1}$    &                    &                   \\
                        & $\recv{4}{1}{0}$   &                   \\
    $\wait{5}{0}{3}$    &                    &                   \\
    $\send{6}{0}{2}$    &                    &                   \\
                        &                    & $\wait{7}{2}{1}$  \\
                        &                    & $\recv{8}{2}{0}$  \\
    $\wait{9}{0}{6}$    &                    &                   \\
    $\send{10}{0}{1}$   &                    &                   \\
                        & $\wait{11}{1}{4}$  &                   \\
                        & $\recv{12}{1}{*}$  &                   \\
    $\wait{13}{0}{10}$  &                    &                   \\
                        & $\wait{14}{1}{11}$ &                   \\
                        &                    & $\wait{15}{2}{8}$ \\
    $\barr{16}{0}{0}$   &                    &                   \\
                        & $\barr{17}{1}{0}$  &                   \\
                        &                    & $\barr{18}{2}{0}$ \\
  \end{tabular}
  \caption{Example CTP with hidden deadlock.}\label{fig:example-deadlock}
\end{figure}

\begin{figure}
  \[\{(6, 9), (9, 10), (10, 4), (4, 11), (11, 12), (12, 1), (1, 7), (7, 8), (8, 6)\}\]
  \caption{Cycle for deadlock in \figref{fig:example-deadlock}.}\label{fig:example-deadlock-cycle}
\end{figure}

\begin{figure}
  \centering
  \begin{tabular}{l|l|l}
    $\send{3}{0}{1}$    &                    &                   \\
                        & $\recv{0}{1}{*}$   &                   \\
    $\wait{5}{0}{3}$    &                    &                   \\
    $\send{6}{0}{2}$    &                    &                   \\
                        & $\wait{2}{1}{0}$   &                   \\
                        & $\recv{4}{1}{0}$   &                   \\
                        &                    & $\send{1}{2}{1}$  \\
    $\wait{9}{0}{6}$    &                    &                   \\
                        & $\wait{11}{1}{4}$  &                   \\
                        &                    & $\wait{7}{2}{1}$  \\
  \end{tabular}
  \caption{Witness execution for deadlock in \figref{fig:example-deadlock-cycle}.}\label{fig:example-deadlock-witness}
\end{figure}

\figref{fig:example-deadlock} shows a feasible complete execution for a simple CTP\@.
Each column is a separate process where actions are listed in $\poeq$ order.
The vertical spacing represents the total order over the actions in the observed execution.
We refer to actions by their unique identifiers.

In this example, the wildcard receive 0 can match with either 1 or 3.
In the complete execution 0 matches with 1, leaving 3 to match with 4.
If this message race is resolved in the opposite direction, matching 0 with 3, a deadlock occurs.

Our analysis detects the deadlock by identifying the program locations $\{\wait{9}{0}{6}, \wait{11}{1}{4},\wait{7}{2}{1}\}$ as a deadlock candidate.
The deadlock candidate is extracted from the cycle shown in \figref{fig:example-deadlock-cycle}.
\figref{fig:example-deadlock-witness} shows the witness execution for this deadlock.
Here 6 is waiting for its only match: 8.
This prevents 10 from being issued and matching with 4, which in turn prevents 12 from being issued and matching with 1.

The graph for \figref{fig:example-deadlock} contains 22 nodes and 36 edges.
Our algorithm detects three deadlock cycles in this graph.
In this case, none of the cycles are filtered away by the abstract machine, even though only one predicts a feasible deadlock.
The other two cycles predict infeasible deadlocks.

The SMT encoding verifies the feasibility of each deadlock cycle by encoding a deadlock at the candidate location.
To find the execution shown in~\figref{fig:example-deadlock-witness} for example, the formula encodes a state where 7, 9 and 11 are issued and where 1, 4 and 6 cannot find any match.
The satisfying assignment for the variables in the formula can be used to construct the witness execution, including the details of which matches were made.

The infeasible cycles report the program locations $\barr{17}{1}{0}$ and $\wait{7}{2}{1}$ in two different candidates.
In other words, they each predict that the program in \figref{fig:example-deadlock} can be re-ordered to reach a state where action 1 is waiting for process 1 to issue a compatible match but process 1 is waiting to finish.
This type of deadlock does occur in some programs and is discussed in \secref{sec:graph}.
However, in this case it is clear that if process 1 reaches action 17, as predicted in the deadlock candidates, either action 0 or 12 must be available to match with action 1, making these candidates infeasible.
It may be possible to extend the abstract machine with some counting logic to filter these candidates but our empirical results (\secref{sec:experiment}) show that it already filters a majority of the cycles detected in our set of benchmarks.

