\section{SMT Encoding}\label{sec:smt}

\begin{figure}
  \begin{tabular}{lc}
    \textsc{Match order} & $\displaystyle \bigwedge_{\act \in \acts_\candidate}\bigwedge_{\act^\prime \in \preimage{\mo}{\act}}(c_{\act} \implies c_{\act^\prime}) \wedge t_{\act^\prime} < t_{\act}$
    \\[1pt]\\
    \textsc{Queue Order} & $\displaystyle \bigwedge_{\act \in \sends{\acts_\candidate} \cup \recvs{\acts_\candidate}}\bigwedge_{\act^\prime \in \preimage{\qo}{\act}}m_{\act^\prime} < m_{\act}$
    \\[1pt]\\
    \textsc{Barriers} & $\displaystyle \bigwedge_{\act \in \barrs{\acts_\candidate}}\bigwedge_{\act^\prime \in \groupacts{\acts_\candidate}{\actgrp{\act}}}t_\act = t_{\act^\prime}$
    \\[1pt]\\
    \textsc{Matches} & $\displaystyle \bigwedge_{\act \in \sends{\acts_\candidate} \cup \recvs{\acts_\candidate}}(c_\act \implies \bigvee_{\act^\prime \in \allmatches_\candidate(\act) \setminus O}MATCH(\act, \act^\prime))$
    \\[1pt]\\
    \textsc{Reach} & $\displaystyle \bigwedge_{\act \in \candidate}\bigwedge_{\act^\prime \in \preimage{\mo}{\act} \setminus O}c_{\act^\prime}$
    \\[1pt]\\
    \textsc{Deadlock} & $\displaystyle \bigwedge_{\act \in \candidate \cup O}\lnot c_\act$
    \\[1pt]\\
    \textsc{No matches} & $\displaystyle \bigwedge_{\act \in O}\bigwedge_{\act^\prime \in \allmatches_\candidate(\act)}c_{\act^\prime}$
  \end{tabular}
  \caption{Constraints in the formula $F$}\label{fig:encoding}
\end{figure}

If $\abstractmachine{\acts_\candidate}$ is able to issue each action in the candidate $\candidate$, then it is used to construct an SMT formula $F$.
A satisfying assignment for $F$ can be used to construct a witness execution for the deadlock candidate.
If $F$ is unsatisfiable, then there is no feasible deadlock state that contains $\candidate$ as part of its control point.
The encoding is an extension of the one designed by Huang et al.~\cite{HuangNFM15} for finding zero-buffer compatible executions of MPI programs.

Let $\allmatches_\candidate$ be an over approximated set of match pairs for $\acts_\candidate$.
For each action $\act \in \acts_\candidate$, the encoding creates an integer variable $t_\act$ to hold its completion timestamp in the witness execution.
If $\act \in \sends{\acts_\candidate} \cup \recvs{\acts_\candidate}$, then the encoding also creates an integer variable $m_\act$ to reference the timestamp of the action that matches $\act$.
Addtionally, $w_\act$ is used as another name for $t_w$ where $w \in \waits{\acts_\candidate}$ and $\act = \actreq{w}$.
The new encoding adds a boolean variable $c_\act$ for every action that is true if $\act$ must be issued and completed in the witness execution.

The rules for the encoding are shown in~\figref{fig:encoding}.
The \textsc{Match order} and \textsc{Queue order} constraints preserve the meaning of $\mo$ and $\qo$ in the encoding.
An action can only complete if its $\mo$ predecessors have completed and the timestamps must reflect that.
Additionally, matches must conform to the non-overtaking guarantee of MPI executions.
In all cases, constraints are omitted when they are obviously redundant with respect to existing constraints and the transitivity of $<$ and $=$ over the integers.

The \textsc{Barriers} constraint encodes the inter-process synchronization behavior of barrier actions by asserting that groups complete at the same time.
All other timestamps, including $m_\act$ for each $\act \in \acts_\candidate$, are asserted to be distinct.

The \textsc{Matches} constraint forces send and receive actions to find a match if they must complete in the witness execution.
Some matches are not allowed assuming the execution ends in a deadlock parked at the actions in $\candidate$.
Specifically, let $O$ denote the set of orphaned send and receive actions for the candidate $\candidate$.
$$O = \{\act \in \acts_\candidate : \exists \act^\prime \in \waits{\acts_\candidate} \cap \candidate, \act = \actreq{\act^\prime} \}$$
The \textsc{Matches} constraint purposely excludes the actions in $O$ as they should not be matched in the witness execution.

In \secref{sec:concrete-semantics}, a send and receive action were matched by copying them from the issued set into the matched set together.
After that, their wait actions were allowed to complete.
This semantic meaning is preserved in the encoding by the $MATCH(\act, \act^\prime)$ constraint which expands to
$$c_{\act^\prime} \wedge t_\act = m_{\act^\prime} \wedge t_{\act^\prime} = m_\act \wedge t_\act < w_{\act^\prime} \wedge t_{\act^\prime} < w_\act.$$

First, this constraint records that $\act^\prime$ is completed by the match.
Second, the actions complete together by recording the timestamp of the other in the $m$ variables.
Finally, both timestamps are required to precede the timestamps of both wait actions.
Note that in the infinite-buffer setting, the wait for the send action does not exist and so one of these constraints is omitted.

The $\textsc{Reach}$ constraint asserts that every predecessor of the actions in $\candidate$ is completed except the actions in $O$.
In other words, it asserts that the deadlock $\candidate$ is reachable.

The $\textsc{Deadlock}$ constraint simply asserts that the deadlock actions in $\candidate$ and the orphaned actions in $O$ are not complete.
The $\textsc{No matches}$ constraint ensures that any issued actions that could untangle the deadlock are complete, thus forcing them to find matches that exclude the deadlock and orphaned actions.
Given a satisfying assignment of the variables in $F$, the witness execution can be constructed from the $t$ timestamp variables while the matches made can be recovered by consulting the $m$ variables.

