
\section{Preliminaries}\label{sec:preliminaries}

\begin{figure}
  \begin{lstlisting}[mathescape]
  a   ::= nb | bb
  nb  ::= $(\mathbf{s}\ \id\ \proc$ src dst$)$
        | $(\mathbf{r}\ \id\ \proc$ src dst$)$
  bb  ::= $\wait{\id}{\proc}{\id}$
        | $\barr{\id}{\proc}{\grp}$
  src ::= $\proc$ | $*$
  dst ::= $\proc$
  \end{lstlisting}
  \caption{Types of Actions a $\in \allacts$.}\label{fig:actions}
\end{figure}

We formalize an observed MPI program execution as a concurrent trace program (CTP).
A CTP is a set of observed communication actions $\acts \subset \allacts$ where $\allacts$ is the set of all possible MPI actions.

The syntax and structure of actions in $\allacts$ is shown in~\figref{fig:actions}.
Each action has a unique identifier $\id \in \nat$ and an owning process $\proc \in \procs{\acts}$ where $\procs{\acts}$ is the set of all processes in the CTP $\acts$.
For a set of actions $X$ and for each process $\proc \in \procs{X}$, $X_{\proc} \subseteq X$ is the projection of the actions in $X$ onto the process $\proc$.

Non-blocking actions are used to asynchronously send (\textbf{s}) and receive (\textbf{r}) messages between processes.
The source and destination processes for a message are indicated by process identifiers in the body of these actions where the source process for a receive can be $*$ to allow for \emph{wildcard} receive. A wildcard receive accepts messages from any source process.

The messages are held in buffers allocated by the user program and copied from source to destination buffers by an MPI runtime.
Data buffers are not explicitly represented in this presentation because the values of messages do not affect the schedules of single-path programs.

Blocking actions are used to halt a process until some condition becomes true, possibly synchronizing two or more processes.
The wait (\textbf{w}) action blocks until the data buffer of a non-blocking action is available (i.e., when data has been copied out of (into) a send (receive) buffer).
The barrier (\textbf{b}) action blocks until each process in a group (indicated with a unique identifier $g$) has reached the same barrier.
Blocking send and receive actions are not included in $\allacts$ because they are accurately modeled by placing a wait action directly after the non-blocking action.

For the remaining sections, we fix a CTP $\acts$.
For simplicity in the presentation of the analysis and accompanying proofs, we assume that the last action in each process $\proc \in \procs{\acts}$ is a barrier action $\barr{\id_\proc}{\proc}{\grp}$ for some group $\grp \in \groups{\acts}$ where $\groups{\acts}$ is the set of barrier groups in $\acts$.
This assumption does not affect the programs we analyze as we can always extend the observed CTP with these actions.
The added barrier action at the end of each process is necessary to model all potential deadlocks in the dependency graph including those arising from wildcard receives.

The disjoint sets of send, receive, wait and barrier actions in $\acts$ are denoted respectively by $\sends{\acts},\recvs{\acts},\waits{\acts},\barrs{\acts}$.
For an action $\act \in \acts$, $\actid{\act}$ and $\actpid{\act}$ denote the action and process identifiers of $\act$.
For an action $\act \in \sends{\acts} \cup \recvs{\acts}$, $\actsrc{\act}$ and $\actdst{\act}$ denote the source and destination process of $\act$.
Of course, $\actsrc{\act} = \actpid{\act}$ and $\actdst{\act} = \actpid{\act}$ for sends and receives respectively.

For an action $\act \in \waits{\acts}$, $\actreq{\act}$ is the non-blocking action that $\act$ waits for.
For an action $\act \in \barrs{\acts}$, $\grp = \actgrp{\act}$ is the group identifier for $\act$ and $\groupacts{\acts}{\grp}$ is the set of actions in that group.

For a process $\proc \in \procs{\acts}$, the actions owned by $\proc$ are always issued sequentially.
This constraint can be captured as a partial order over $\acts$ if we assume that action identifiers were assigned in ascending order while observing the original execution.

\begin{definition}[Process order]\label{def:process-order}
  Process order is a partial order $(\acts, \poeq^{\acts})$ where
  $$\forall \act,\act^\prime \in \acts : \act \poeq^{\acts} \act^\prime \iff \actpid{\act} = \actpid{\act^\prime} \wedge \actid{\act} \leq \actid{\act^\prime}$$
\end{definition}

For~\defref{def:process-order} and similarly for any other partial order defined in this paper, we use $\po^{\acts}$ to mean the partial order with reflexivity removed and we omit $\acts$ from the notation when it is clear from context.

