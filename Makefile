all:
	pdflatex mpi-deadlock.tex
	bibtex mpi-deadlock
	pdflatex mpi-deadlock.tex
	pdflatex mpi-deadlock.tex

clean:
	rm -f *.pdf *.aux *.log *.out *.bbl *.blg *.fls *.fdb_latexmk *.lof *.lol *.lot *.toc

proofs:
	pdflatex "\def\INCLUDEAPPENDIX{1} \input{mpi-deadlock.tex}"
	bibtex mpi-deadlock
	pdflatex "\def\INCLUDEAPPENDIX{1} \input{mpi-deadlock.tex}"
	pdflatex "\def\INCLUDEAPPENDIX{1} \input{mpi-deadlock.tex}"

