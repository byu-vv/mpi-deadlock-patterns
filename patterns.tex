
\section{Programs without Barriers}\label{sec:patterns}

\begin{figure}
  \centering
  \begin{tabular}{l|l|l}
    0 & 1 & 2 \\
    \hline
    $\send{4}{0}{1}$ & $\recv{1}{1}{*}$ & $\send{0}{2}{1}$ \\
                     & $\wait{3}{1}{1}$ & $\wait{2}{2}{0}$ \\
    $\barr{5}{0}{0}$ & $\barr{6}{1}{0}$ & $\barr{7}{2}{0}$ \\
    $\wait{8}{0}{4}$ & $\recv{9}{1}{0}$ &                  \\
                     & $\wait{10}{1}{8}$ &                  \\
    $\barr{11}{0}{1}$ & $\barr{12}{1}{1}$ & $\barr{13}{2}{1}$ \\
  \end{tabular}
  \caption{Example CTP with Barrier.}\label{fig:ctp-barrier-example}
\end{figure}

In order to limit the number of cycles in the dependency graph we have explored characterizing these cycles as instances of two distinct \emph{deadlock patterns}.
One of these patterns can detect deadlocks caused by message starvation.
The analysis can then proceed with a \emph{reduced dependency graph} that does not include edges created by rules three and four in~\defref{def:edges}.

\begin{definition}[Circular dependency pattern]\label{def:circular-dependency-pattern}
  An instance of the circular dependency pattern is a partial control point extracted from a cycle $\cycle \in \pow{\edges}$ in the reduced dependency graph.
\end{definition}

\begin{definition}[Orphaned receive pattern]\label{def:orphaned-receive-pattern}
  An instance of the orphaned receive pattern is a pair of receive actions $\act, \act^\prime \in \recvs{\acts}$ where
  \begin{itemize}
    \item $\act \po \act^\prime$,
    \item $\actsrc{\act} = *$,
    \item $\actsrc{\act^\prime} \neq *$ and
    \item $\exists \act^{\prime\prime} \in \allmatches(\act), \actsrc{\act^{\prime\prime}} = \actsrc{\act^\prime}$.
  \end{itemize}
\end{definition}

In~\defref{def:orphaned-receive-pattern} the partial control point used to characterize candidate deadlock states is the single action $\act^\prime$.
Instances of the orphaned receive pattern are trivially computed using counting, further improving the performance of the analysis.
The following theorem states that instances of the orphaned receive pattern do detect all deadlocks caused by message starvation in the special case that the CTP does not contain barrier actions (excluding the final group added by the analysis).

\begin{theorem}[Orphaned receive pattern sound]\label{thm:orphaned-receive-pattern-sound}
  Let $\acts$ be a CTP where $\barrs{\acts}$ consists solely of the final barrier actions added by the analysis.
  Let $\astate = \statetuple{\acts}{\issued}{\matched}$ be a reachable state of $\acts$ such that $\astate \in \reachablestates{\acts}{\concretet}$ and $\deadlocked{\concretet}{\astate}$.
  Let $\candidate = \extractcandidate{\astate}$ be the full control point of $\astate$ and let $\act \in \waits{\candidate}$ be an arbitrary wait action at the control point.
  If no potential match for $\actreq{\act}$ exists in $\acts \setminus \issued$, then there must exist a pair of receive actions $r_*, r \in \recvs{\acts}$ that satisfy~\defref{def:orphaned-receive-pattern}.
\end{theorem}

A proof of this theorem is left for future work.
We also believe the orphaned receive pattern can be adapted to support CTPs with barriers.

\figref{fig:ctp-barrier-example} shows a CTP with a deadlock that the two pattern instances cannot detect.
We refer to the actions in~\figref{fig:ctp-barrier-example} by their identifiers.

Action 1 can match with either action 0 or action 4.
In the case that action 1 matches with action 4, the CTP deadlocks at actions 5, 6 and 2.
The orphaned receive pattern correctly identifies that action 9 is starved in this deadlock but the control point never reaches action 9 so the deadlock that \emph{includes} action 9 is infeasible.
Furthermore, the cycles in the reduced dependency graph do not detect the dependency between action 0 and process 1 in the case that action 0 is starved.

