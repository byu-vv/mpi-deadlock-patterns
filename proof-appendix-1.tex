
\subsection{Proof of~\thmref{thm:deadlock-candidates-sound}}\label{sec:proof-deadlock-candidates-sound}

First we prove a few useful lemmas.
Fix a reachable deadlock $\astate = \statetuple{\acts}{\issued}{\matched}$ and its full control point $\candidate = \extractcandidate{\astate}$.

\begin{lemma}\label{lem:candidate-blocking}[deadlock-actions-blocking]
  $$\candidate \subseteq \waits{\acts} \cup \barrs{\acts}$$
\end{lemma}
\begin{proof}
  Proof by contradiction.
  Assume that $\exists \act \in \candidate, \act \notin \waits{\acts} \cup \barrs{\acts}$.

  Then $\act \in \sends{\acts} \cup \recvs{\acts}$.
  Let $\act^\prime$ be the very next action after $\act$ in $\acts_{\actpid{\act}}$.
  $\act^\prime$ must exist since $\act$ is a send or receive action and the last action in every process is a barrier action.

  By definition $\candidate$ contains the last issued action of every process in $\procs{\acts}$.
  It follows that $\act \in \issued$ and $\act^\prime \not\in \issued$.
  Furthermore, by the definition of deadlock, there is no transition enabled in $\astate$ that can issue $\astate^\prime$.

  The definition of $\concretet$ requires that every action preceding $\act$ in $\acts_{\actpid{\act}}$ must be issued and every blocking action preceding $\act$ must be matched in $\astate$.
  Because $\act \in \issued$ and $\act \in \sends{\acts} \cup \recvs{\acts}$, these conditions also hold for $\act^\prime$.
  Therefore $\act^\prime$ can be issued.
  This contradicts the deadlock of $\astate$.
  \qed
\end{proof}

\begin{lemma}\label{lem:send-no-match-wildcard}[wildcard-causes-send-starvation]
  Let $\act \in \candidate \cap \waits{\acts}$ be a wait action participating in the deadlock at $\astate$ such that $\actreq{\act} \in \sends{\acts}$.
  If there are no receive actions in $(\recvs{\acts} \setminus \issued)_{\actdst{\actreq{\act}}}$ (no unissued potential matches),
  then there must exist an issued wildcard receive action $\act^\prime \in (\recvs{\acts} \cap \issued)_{\actdst{\actreq{\act}}}, \actsrc{\act^\prime} = *$ in the destination process.
\end{lemma}
\begin{proof}
  Proof by contradiction.
  Assume that there is no receive $\act^\prime \in \recvs{\acts} \cap \issued$ such that $\actpid{\act^\prime} = \actdst{\actreq{\act}}$ and $\actsrc{\act^\prime} = *$.
  Then the number of send and receive match pairs between $\actsrc{\actreq{\act}}$ and $\actdst{\actreq{\act}}$ is fixed in every execution of $\acts$.
  Since $\astate$ is deadlocked and there are no unissued potential matches for $\actreq{\act}$, it follows that $|(\sends{\acts} \cap \issued)_{\actsrc{\actreq{\act}}}| > |\recvs{\acts}_{\actdst{\actreq{\act}}}|$.
  Therefore, $\acts$ will deadlock deterministically at $\act$.
  This contradicts the fact that $\acts$ was obtained by observing a successful execution of an MPI program.
  \qed
\end{proof}

\begin{lemma}\label{lem:recv-no-match-wildcard}[wildcard-causes-recv-starvation]
  Let $\act \in \candidate \cap \waits{\acts}$ be a wait action participating in the deadlock at $\astate$ such that $\actreq{\act} \in \recvs{\acts} \wedge \actsrc{\act} \neq *$.
  If there are no send actions in $(\sends{\acts} \setminus \issued)_{\actsrc{\actreq{\act}}}$ (no unissued potential matches),
  then there must exist an issued wildcard receive action $\act^\prime \in (\recvs{\acts} \cap \issued)_{\actpid{\act}}, \actsrc{\act^\prime} = *$ in the destination process.
\end{lemma}
\begin{proof}
  Proof by contradiction.
  Assume that there is no receive $\act^\prime \in \recvs{\acts} \cap \issued$ such that $\act^\prime \po \actreq{\act}$ and $\actsrc{\act^\prime} = *$.
  Then the number of send and receive match pairs between $\actsrc{\actreq{\act}}$ and $\actdst{\actreq{\act}}$ is fixed at least until $\act$ (where after $\act$ there may be more receives but no more sends).
  Since $\astate$ is deadlocked and there are no unissued potential matches for $\actreq{\act}$, it follows that $|(\recvs{\acts} \cap \issued)_{\actdst{\actreq{\act}}}| > |\sends{\acts}_{\actsrc{\actreq{\act}}}|$.
  Therefore, $\acts$ will deadlock deterministically at $\act$.
  This contradicts the fact that $\acts$ was obtained by observing a successful execution of an MPI program.
  \qed
\end{proof}

\begin{lemma}\label{lem:blocking-path}[parent-action-exists]
  $$\forall \act \in \candidate, (\exists \act^\prime \in \acts \setminus \issued, \actpid{\act^\prime} \neq \actpid{\act} \wedge (\act^\prime, \act) \in \edges^*)$$
\end{lemma}
\begin{proof}
  Proof by case analysis on the action type of $\act$.
  By~\lemmaref{lem:candidate-blocking}, $\act \in \waits{\acts} \cup \barrs{\acts}$.

  In the first case, $\act \in \waits{\acts} \wedge \actreq{\act} \in \sends{\acts}$
  If there is an unissued potentially matching receive action $\act^\prime$ in $(\acts \setminus \issued)_{\actdst{\actreq{\act}}}$, then rule two will ensure that $(\act^\prime, \actreq{\act}) \in \edges$.
  If there is no such matching receive, then $\act^\prime = \nocomm_{\actdst{\actreq{\act}}}$.
  By~\lemmaref{lem:send-no-match-wildcard}, there must be a wildcard receive in $\issued_{\actdst{\actreq{\act}}}$.
  Therefore, rule four will again ensure that $(\act^\prime, \actreq{\act}) \in \edges$.
  In either case, because $\actreq{\act} \mo \act$, it follows that $(\actreq{\act}, \act) \in \edges$ and thus $(\act^\prime, \act) \in \edges^*$.

  In the second case, $\act \in \waits{\acts} \wedge \actreq{\act} \in \recvs{\acts}$
  If there is an unissued potentially matching send action $\act^\prime$ in $(\acts \setminus \issued)_{\actsrc{\actreq{\act}}}$, then rule two will ensure that $(\act^\prime, \actreq{\act}) \in \edges$.
  If there is no such matching send, then $\act^\prime = \nocomm_{\actsrc{\actreq{\act}}}$.
  By~\lemmaref{lem:recv-no-match-wildcard}, there must be a wildcard receive in $\issued_{\actpid{\act}}$.
  Therefore, rule three will again ensure that $(\act^\prime, \actreq{\act}) \in \edges$.
  In either case, because $\actreq{\act} \mo \act$, it follows that $(\actreq{\act}, \act) \in \edges$ and thus $(\act^\prime, \act) \in \edges^*$.

  In the final case, $\act \in \barrs{\acts}$
  Then $\act^\prime \in \barrs{\acts} \setminus \issued$ and rule two ensures that $(\act^\prime, \act) \in \edges$.
  Such an $\act^\prime$ must exist, otherwise $\act$ could be completed and $\astate$ would not be a deadlock.

  In the first two cases, $\actreq{\act}$ is the orphaned action, in the last case, $\act$ itself is the orphaned action.
  \qed
\end{proof}

\begin{lemma}\label{lem:candidate-path}[deadlock-actions-connected]
  $$\forall \act \in \candidate, (\exists \act^\prime \in \candidate, \act^\prime \neq \act, (\act^\prime, \act) \in \edges^*)$$
\end{lemma}
\begin{proof}
  Direct proof.
  By~\lemmaref{lem:blocking-path}, there exists some action $\act^{\prime\prime} \in \acts \setminus \issued$ such that $\actpid{\act^{\prime\prime}} \neq \actpid{\act}$ and $(\act^{\prime\prime}, \act) \in \edges^*$.
  By definition, $\candidate$ contains an action $\act^\prime \in \issued_{\actpid{\act^{\prime\prime}}}$.
  It follows that $\act^\prime \poeq \act^{\prime\prime}$.

  If $\act^\prime \po \act^{\prime\prime}$, then $\act^\prime \mo \act^{\prime\prime}$ since $\act^\prime$ is a blocking action by~\lemmaref{lem:candidate-blocking}.
  In this case, rule one ensures that $(\act^\prime, \act^{\prime\prime}) \in \edges^*$.
  And by the transivity of $\edges^*$, $(\act^\prime, \act) \in \edges^*$.

  In the second case, $\act^\prime = \act^{\prime\prime} = \nocomm_{\actpid{\act^\prime}}$.
  This is a contradiction since $\act^\prime \in \issued$ by the definition of $\candidate$ and $\act^{\prime\prime} \not\in \issued$ by~\lemmaref{lem:blocking-path}.
  \qed
\end{proof}

Now we can prove~\thmref{thm:deadlock-candidates-sound}.
By~\lemmaref{lem:candidate-path}, there exists some action $\act^\prime \in \candidate$ such that $\act \neq \act^\prime$ and $(\act^\prime, \act) \in \edges^*$.
By the same argument, there exists an action $\act^{\prime\prime} \in \candidate$ such that $\act^\prime \neq \act^{\prime\prime}$ and $(\act^{\prime\prime}, \act^\prime) \in \edges^*$.
If $\act = \act^\prime$, then we have a cycle $\cycle = \{ (\act, \act^\prime), \ldots, (\act^\prime, \act) \}$.
Otherwise, we can continue applying~\lemmaref{lem:candidate-path} until a cycle is created between the actions in $\candidate$.

Let $d$ be the partial control point or candidate extracted from $\cycle$ by taking the earliest blocking action in $\cycle$ from every process with an action in $\cycle$.
The actions that make up edges in $\cycle$ are completely enumerated in Lemmas~\ref{lem:blocking-path} and~\ref{lem:candidate-path}.
$\cycle$ is entirely made up of actions in $\candidate$, their message requests (which are non-blocking), unissued matches and final barrier actions.
Therefore, $d \subseteq \candidate$ holds by construction.
\qed

