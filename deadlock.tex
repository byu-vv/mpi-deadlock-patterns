\section{Deadlock}\label{sec:deadlock}

This section formalizes the problem statement for detecting deadlock in $\acts$\@.
The definitions are given in terms of a generic transition relation $\delta \subseteq \statespace \times \statespace$ because we reuse them later with the abstract transistion relation. 

Let $\reachablestates{\astate}{\delta} \subseteq \statespace$ denote the reachable states of $\acts$ from the state $\astate$ with respect to a transition relation $\delta$:
$$\reachablestates{\astate}{\delta} = \{ \astate^\prime \in \statespace : (\astate, \astate^\prime) \in \delta^* \}$$
where $\delta^*$ denotes the transitive closure of $\delta$.

\begin{definition}[Deadlock]\label{def:deadlock}
  A state $\astate = \statetuple{\acts}{\issued}{\matched}$ is deadlocked with respect to a transition relation $\delta$ if there are no enabled transitions and there are actions left to be issued or matched:
  $$\deadlocked{\delta}{\astate} \iff (\issued \neq \acts \vee \matched \neq \acts) \wedge (\forall \astate^\prime \in \statespace, (\astate, \astate^\prime) \not\in \delta)$$
\end{definition}

The deadlock discovery problem, given in~\defref{def:all-deadlock-problem}, asks whether $\machine{\acts}$ can reach a deadlocked state.
This problem is NP-Complete and can be directly encoded as a propositional formula~\cite{DBLP:journals/toplas/ForejtJKNS17}.

\begin{definition}[Deadlock discovery problem]\label{def:all-deadlock-problem}
  $$\exists \astate \in \reachablestates{\astate_0}{\delta}, \deadlocked{\delta}{\astate}$$
\end{definition}

The search for an arbitrary feasible deadlock state can be extremely expensive for many programs.
We can give the search a kind of head start by finding a simple way to characterize the types of states that may deadlock.
A convenient way to describe a state is by its \emph{control point}.
The control point of a state is simply the set of last issued actions from each process:
$$\extractcandidate{\statetuple{\acts}{\issued}{\matched}} = \{ \act \in \issued : \forall \act^\prime \in \issued_{\actpid{\act}}, \act^\prime \poeq \act \}.$$

If we only provide the last issued action for a subset of process, we obtain a partial control point that describes the collection of states which include it as a subset of their control points.
\defref{def:single-deadlock-problem} augments the problem statement in~\defref{def:all-deadlock-problem} to ask for a deadlock state that matches a partial program point $\candidate$ (also called a deadlock candidate).

\begin{definition}[Constrained deadlock discovery problem]\label{def:single-deadlock-problem}
  $$\exists \astate \in \reachablestates{\astate_0}{\delta}, \candidate \subseteq \extractcandidate{\astate} \wedge \deadlocked{\delta}{\astate}$$
\end{definition}
