
\section{Dependency Graph}\label{sec:graph}

This section presents a technique for generating a sound set of deadlock candidates $\candidates{\acts}$ for $\acts$.
The soundness property according to~\defref{def:single-deadlock-problem} is formally stated in the following theorem.

\begin{theorem}[Deadlock candidates sound]\label{thm:deadlock-candidates-sound}
  For all states $\astate \in \reachablestates{\astate_0}{\concretet}$,
  If $\deadlocked{\concretet}{\astate}$, then $\exists d \in \candidates{\acts}, d \subseteq \extractcandidate{\astate}$.
\end{theorem}

We generate $\candidates{\acts}$ by detecting cycles in a graph $(\nodes, \edges)$ where $\nodes = \acts \cup \{\nocomm_\proc : \proc \in \procs{\acts} \}$ is the set of nodes and $\edges: \nodes \times \nodes$ is the set of edges.
The node $\nocomm_\proc$ is used to explicitly represent the end of process $\proc$ in the graph.
An edge $(\act, \act^\prime) \in \edges$ represents a potential communication dependency of $\act^\prime$ on $\act$ in some execution of $\acts$.
\ifdefined\INCLUDEAPPENDIX
A proof that our technique satisfies~\thmref{thm:deadlock-candidates-sound} is given in the appendix along with proofs for the other theorems stated in this paper.
\else
A proof that our technique satisfies~\thmref{thm:deadlock-candidates-sound}, along with proofs for the other theorems stated in this paper, is given in the appendix of the full paper~\cite{paper-long}.
\fi

Before presenting the dependency graph, we describe how one of its cycles can represent a deadlock.
This not only motivates the rules for adding edges to the graph, but also leads to a precise understanding of the type of cycle the analysis must report and the types it can ignore.
This is important because the graphs we build may contain a huge number of cycles which are expensive to enumerate.
The more we can ignore, the more efficient our analysis will be.

Fix a deadlock state $\astate \in \reachablestates{\astate_0}{\concretet}$ with control point $\candidate = \extractcandidate{\astate}$.
For each process $\proc \in \procs{\acts}$, there is an action $\act \in \candidate$ with $\proc = \actpid{\act}$.
We will define a few new terms that allow us to talk about why $\act$ is blocking $\proc$ from progressing in the state $\astate$.

First, we call $\act$ the \emph{deadlock action} for $\proc$.
Next, let $\act^\prime$ be the earliest action in $\proc$ that is issued in $\astate$ but not matched with $\act^\prime \moeq \act$.
We call $\act^\prime$ the \emph{orphaned action} for $\proc$.
Finally let $\act^{\prime\prime}$ be an action that is not issued in $\astate$ but would allow $\proc$ to progress if it is matched with $\act$.
We call $\act^{\prime\prime}$ the \emph{parent action}.
If the parent action does not exist, it is represented in the graph by a $\bot$ node (discussed more below).
We will use the following definition to translate these concepts to the context of a path of edges in $\edges$.

\begin{definition}[Deadlock path]\label{def:deadlock-path}
  Let $(\act_0, \act_1), \ldots, (\act_{n-1}, \act_n)$ be a path of edges in $\edges$.
  This path is a \emph{deadlock path} for the process $\proc \in \procs{\acts}$ if
  \begin{enumerate}
    \item $\actpid{\act_0} \neq \proc$ and
    \item $\actpid{\act_i} = \proc$ for all $i \in \{1 \ldots n\}$ and
    \item for some $i \in \{1 \ldots n - 1\}$, $\act_i \in \waits{\acts} \cup \barrs{\acts}$.
  \end{enumerate}
\end{definition}

In a deadlock path $(\act_0, \act_1), \ldots, (\act_{n-1}, \act_n)$ for the process $\proc$, $\act_0$ and $\act_1$ are interpreted as parent and orphan actions of $\proc$.
The edge connecting them represents the possibility that $\act_1$ may depend on $\act_0$ being issued and available to match to complete in some execution.
The earliest blocking action issued by $\proc$ and contained in the path is interpreted as the deadlock action of $\proc$.

\defref{def:deadlock-path} requires that the deadlock action not be the last action in the path.
This is because a deadlock cycle is constructed by composing the deadlock paths of two or more processes.
In other words $\act_n$ in the deadlock path of $\proc$ will be the parent action in a different deadlock path for some process $\proc^\prime \neq \proc$.
The deadlock action cannot also be a parent action because the deadlock action is issued and ready to match by definition.

\begin{definition}[Deadlock cycle]\label{def:deadlock-cycle}
  A cycle of edges in $\edges$ is a \emph{deadlock cycle} if it can be constructed from a set of deadlock paths where each process in the program contributes at most one path.
  Additionally, the orphaned action of each deadlock path must not be a compatible match for the orphaned action of any other deadlock path in the cycle.
  Otherwise, the potential deadlock state would quickly unwind by matching the two issued orphaned actions.
\end{definition}

The candidate of a deadlock cycle is the set of deadlock actions from its paths.
We construct $\candidates{\acts}$ by extracting the candidate from each deadlock cycle in the graph.
The proof of~\thmref{thm:deadlock-candidates-sound}, referred to the full paper \cite{paper-long}, shows that for every feasible deadlock state, there is a corresponding cycle in $\edges$ that conforms to~\defref{def:deadlock-cycle} and yields a matching partial control point.
The first step to constructing $\edges$ is to know which actions can match together in an execution of $\acts$.

\begin{definition}[Potential matches]\label{def:potential-matches}
  For an action $\act \in \acts$, $\allmatches(\act)$ denotes the set of actions that can be matched with $\act$ in some transition between reachable states of $\acts$.
  More precisely, if $\matched(\astate)$ denotes the matched set in the state $\astate$, then
  $$\allmatches(\act) = \{ \act^\prime : \exists \astate,\astate^\prime \in \reachablestates{\astate_0}{\concretet}, \astate \concretet \astate^\prime \wedge \{\act, \act^\prime\} \subseteq \matched(\astate^\prime) \setminus \matched(\astate) \} \setminus \{\act\}.$$
\end{definition}

Note that fully determining $\allmatches$ is as hard as the deadlock discovery problem itself.
Fortunately, $\allmatches$ must only be over approximated to ensure that the whole analysis is sound.
A simple over approximation would include every compatible match of the action $\act$ in $\allmatches(\act)$.
However, the analysis presented in this paper uses the more precise approximation given in~\cite{huang2020efficient} which results in fewer spurious cycles.

\begin{definition}[Edges]\label{def:edges}
  An edge $(\act, \nocomm_{\actpid{\act}})$ is added to $\edges$ for every $\act \in \acts$.
  Other edges are added between two actions $\act,\act^\prime \in \acts$ according to the following rules:
  \begin{enumerate}
    \item If $\act \mo \act^\prime$ then $(\act, \act^\prime) \in \edges$.
    \item If $\act^\prime \in \allmatches(\act)$, then $(\act, \act^\prime), (\act^\prime, \act) \in \edges$.
    \item If $\act,\act^\prime \in \recvs{\acts}$ such that $\act \po \act^\prime \wedge \actsrc{\act} = * \wedge \actsrc{\act^\prime} \neq *$, then $(\nocomm_{\actsrc{\act^\prime}}, \act^\prime) \in \edges$.
    \item If $\act \in \sends{\acts}$ and $\act^\prime \in \recvs{\acts}$ such that $\actdst{\act} = \actdst{\act^\prime}$ and $\actsrc{\act^\prime} = *$, then $(\nocomm_{\actdst{\act}}, \act) \in \edges$.
  \end{enumerate}
\end{definition}

Rule one of~\defref{def:edges} is an obvious preservation of the semantics presented in~\secref{sec:concrete-semantics}.
It is necessary for connecting deadlock actions to other actions in the tail of a deadlock path.
It also ensures that wait actions (often a deadlock action) have an incoming edge from their associated message request (often an orphaned action).

The rest of the rules ensure that parent actions are connected to orphan actions in any scenario.
The most obvious occurs in rule two when the actions can form a match.
The edge is bidirectional because we don't know which action will be the orphaned action and which will be the parent action in a given deadlock.

Rules three and four add edges to encode the possibility of \emph{message starvation}.
Message starvation occurs when wildcard receive actions are matched with send actions in an unintended way that leaves subsequent send and receive actions without any potential future matches.
In this type of deadlock the parent action does not exist in the program (the orphaned action is waiting for an action that will never be issued).
The added $\bot$ node at the end of every process takes the place of the parent action in these cases.

Note that we do not need a rule for adding an edge from $\bot$ nodes to wildcard receive actions because if a wildcard receive action is starved, then the resulting deadlock would have been deterministic.
There is no need for analysis in this case, as the deadlock occurs no matter how you schedule the program.

These rules will inevitably generate graphs with many spurious cycles that obviously do not correspond to real deadlock states.
For example, the second rule alone forms a trivial cycle between two potentially matching actions.
We only want to enumerate cycles that match~\defref{def:deadlock-cycle}.

\begin{algorithm}
  \caption{Determine whether the edge $(v, w)$ can possibly reach a deadlock cycle starting at $s$}\label{alg:deadlock-edge}
  \begin{algorithmic}[1]
    \Procedure{DeadlockEdge}{$v,w,s$}
    \If{$\actpid{v} = \actpid{w}$}
    \State \Return $\mathit{true}$
    \EndIf
    \If{$\mathtt{block\_count}(\mathtt{stack}$, $\actpid{v}) = 0$}
    \State \Return $\mathit{false}$
    \EndIf
    \If{$\mathtt{block\_count}(\mathtt{stack}, \actpid{v}) = 1 \wedge v \in \waits{\acts} \cup \barrs{\acts}$}
    \State \Return $\mathit{false}$
    \EndIf
    \If{$\exists a \in \mathtt{orphaned}(\mathtt{stack}), \mathtt{can\_match}(a, w)$}
    \State \Return $\mathit{false}$
    \EndIf
    \If{$w \neq s$}
    \State \Return $\forall a \in \mathtt{stack}, \actpid{a} \neq \actpid{w}$
    \EndIf
    \State \Return $\mathit{false}$
    \EndProcedure
  \end{algorithmic}
\end{algorithm}

By default, Johnson's algorithm for enumerating the elementary cycles of a directed graph~\cite{DBLP:journals/siamcomp/Johnson75} will enumerate all of the spurious cycles.
\algoref{alg:deadlock-edge} presents a simple boolean function that can be added to Johnson's algorithm to ensure only deadlock cycles are visited.

Johnson's algorithm searches for cycles in one strongly connected component at a time according to a user-defined order.
We extend $\poeq$ to a total order over the nodes in the graph.
The component containing the least node $s$ is searched first and $s$ is visited first.
The global variable $\mathtt{stack}$ maintains a normal depth first search stack of visited nodes.

The result of the chosen order is that $s$ is always the orphaned action of the first deadlock path visited by the algorithm.
Our specialized depth first search visits the successor $w$ of the current node $v$ when DeadlockEdge($v, w, s$) returns $\mathit{true}$.
This ensures that the stack is always extending to a valid deadlock path according to~\defref{def:deadlock-path}.

The $\mathtt{block\_count}$ function returns the number of blocking actions in a given process that have been visited by the current stack.
The $\mathtt{can\_match}$ function determines whether two actions may form a compatible match based on their types and endpoints.
Finally, the $\mathtt{orphaned}$ function returns the orphaned action from each deadlock path in the current stack.

Lines 2-3 of~\algoref{alg:deadlock-edge} allow the tail of the current deadlock path on the stack to be extended along the same process (rule one in \defref{def:deadlock-path}).
If the condition on line 2 is false, then $v$ and $w$ are the parent and orphaned actions for a new deadlock path.
Lines 4-7 ensure that the current deadlock path contains a deadlock action that is not also a parent action for $w$ (rules two and three in \defref{def:deadlock-path}).
Lines 8-9 ensure that orphaned actions cannot match other orphaned actions (\defref{def:deadlock-cycle}).
Lines 10-11 ensure that each process only contributes one path to the cycle (also \defref{def:deadlock-cycle}).
The final case occurs when $w$ is equal to $s$ and a cycle is formed.

In our implementation, we also ensure that duplicate deadlock candidates are not reported by keeping track of how actions are orphaned.
If the tail of a long deadlock path can reach an orphan action in another process from two different parent actions, it is simple to only enumerate one such deadlock cycle.
This is left out of~\algoref{alg:deadlock-edge} for clarity.

