\section{Abstract Semantics of Concurrent Trace Programs}\label{sec:abstract-semantics}

\begin{figure}
  \centering
  \issuesendt\ Transition
  \begin{tabular}{>{\centering\arraybackslash}p{\linewidth}}
    \\[\sep]
    $\act \in \sends{\acts} \setminus \issued$\ \ \ \ $\preimage{\po}{\act} \subseteq \issued$\ \ \ \ $(\preimage{\po}{\act} \cap (\waits{\acts} \cup \barrs{\acts})) \subseteq \matched$ \\
    $s = \sendfull{(\actid{\act} - 1)}{\actpid{\act}}{*}{\actdst{\act}}$\\
    \hline
    $\statetuple{\acts}{\issued}{\matched} \concretet \statetuple{\acts \cup \{s\}}{\issued \cup \{s, \act\}}{\matched}$
    \\[\sep]\\
  \end{tabular}

  \issueothert\ Transition
  \begin{tabular}{>{\centering\arraybackslash}p{\linewidth}}
    \\[\sep]
    $\act \in \acts \setminus \issued$\ \ \ \ $\act \not\in \sends{\acts}$\ \ \ \ $\preimage{\po}{\act} \subseteq \issued$\ \ \ \ $(\preimage{\po}{\act} \cap (\waits{\acts} \cup \barrs{\acts})) \subseteq \matched$ \\
    \hline
    $\statetuple{\acts}{\issued}{\matched} \concretet \statetuple{\acts}{\issued \cup \{\act\}}{\matched}$
    \\[\sep]\\
  \end{tabular}

  \matchsendrecvt\ Transition
  \begin{tabular}{>{\centering\arraybackslash}p{\linewidth}}
    \\[\sep]
    $\act,\act^\prime \in (\issued \setminus \matched)$\ \ \ \ $\act \in \sends{\acts}$\ \ \ \ $\act^\prime \in \recvs{\acts}$\ \ \ \ $\actdst{\act} = \actdst{\act^\prime}$\ \ \ \ $\actsrc{\act^\prime} = \actsrc{\act}$\ \ \ \ $(\preimage{\mo}{\act} \cup \preimage{\mo}{\act^\prime}) \subseteq \matched$ \\
    \hline
    $\statetuple{\acts}{\issued}{\matched} \abstractt \statetuple{\acts}{\issued}{\matched \cup \{\act, \act^\prime\}}$
    \\[\sep]\\
  \end{tabular}

  \matchwaitt\ Transition
  \begin{tabular}{>{\centering\arraybackslash}p{\linewidth}}
    \\[\sep]
    $\act \in \issued \setminus \matched$\ \ \ \ $\act \in \waits{\acts}$\ \ \ \ $s = \sendfull{(\actid{\actreq{\act}} - 1)}{\actpid{\actreq{\act}}}{*}{\actdst{\actreq{\act}}}$\ \ \ \ $\actreq{\act} \in \matched \vee (\actreq{\act} \in \sends{\acts} \wedge s \in \matched)$ \\
    \hline
    $\statetuple{\acts}{\issued}{\matched} \abstractt \statetuple{\acts}{\issued}{\matched \cup \{\act\}}$
    \\[\sep]\\
  \end{tabular}
  \caption{Transition Rules for Abstract Semantics.}\label{fig:abstract}
\end{figure}

This section defines an abstract machine $\abstractmachine{\acts} = \machinetuple{\statespace}{\astate_0}{\abstractt}$ that augments the semantics of CTPs to efficiently filter away infeasible deadlock candidates.
This filtering is an important stage in the analysis because it can drastically reduce the number of calls to the SMT solver.
The abstract transition relation $\abstractt$ is shown in~\figref{fig:abstract} with the barrier transition omitted as it is unchanged from $\concretet$.
In this transition relation we create a dedicated \emph{wildcard endpoint} for each source process.
This eliminates the possibility of message starvation.

The \issuesendt\ transition generates a fresh \emph{wildcard send} for the new endpoint and issues it alongside the original send action.
A wait on the original send action is allowed to match in the \matchwaitt\ transition if either the original send \emph{or} the wildcard send has been matched.
The wildcard send is only allowed to match wildcard receive actions issued by the destination process and the original send action is only allowed to match deterministic receive actions.

We filter a deadlock candidate $\candidate$ by deriving a CTP $\acts_\candidate$ that contains the actions in $\candidate$, the actions process ordered before actions in $\candidate$ and all of the actions in other processes:

$$\acts_\candidate = \bigcup_{\act \in \candidate}\preimage{\poeq}{\act} \cup \bigcup_{\proc \notin \procs{\candidate}}\acts_\proc$$

We then attempt to execute $\abstractmachine{\acts_\candidate}$ to determine whether

$$\exists \astate \in \reachablestates{\astate_0}{\abstractt}, \candidate \subseteq \extractcandidate{\astate} \wedge \deadlocked{\abstractt}{\astate}$$

Note that this is just~\defref{def:single-deadlock-problem} with the abstract transition relation substituted in.
If the abstract execution is able to issue every action in $\candidate$, then the candidate may represent a real deadlock and it is added to the set of candidates to be encoded as an SMT formula.
Otherwise, the candidate is infeasible and is discarded.

The abstract machine is sound if it never discards a reachable control point.
Let the set of reachable control points from a state $\astate$ and a transition relation $\delta$ be $\reachablectrls{\astate}{\delta}$.

$$\reachablectrls{\astate}{\delta} = \{ \extractcandidate{\astate^\prime} : \astate^\prime \in \reachablestates{\astate}{\delta} \}$$

\thmref{thm:abstract-candidate-simulation} states that the reachable control points of the abstract machine subsumes the reachable control points of the concrete machine.
\thmref{thm:abstract-deadlock-deterministic} states that if the abstract machine cannot issue every action in the deadlock candidate in one execution, it will not be able to issue them all in any execution.
Together these theorems prove that the candidate $\candidate$ can be filtered away in a single execution of the abstract machine when it fails to issue all of the actions in $\candidate$.

\begin{theorem}[Abstract candidate simulation]\label{thm:abstract-candidate-simulation}
  Let $\astate \in \reachablestates{\astate_0}{\concretet}$ and $\astate^\prime \in \reachablestates{\astate_0}{\abstractt}$ be a concrete and abstract state reachable from the start state $\astate_0$.
  If $\extractcandidate{\astate} = \extractcandidate{\astate^\prime}$, then for all control points $\candidate \in \reachablectrls{\astate}{\concretet}$, it follows that $\candidate \in \reachablectrls{\astate^\prime}{\abstractt}$.
\end{theorem}

\begin{theorem}[Abstract deadlock deterministic]\label{thm:abstract-deadlock-deterministic}
  Let $\astate \in \reachablestates{\astate_0}{\abstractt}$ be a reachable abstract state with $\deadlocked{\abstractt}{\astate}$.
  If $\candidate \not\subseteq \extractcandidate{\astate}$, then for all $\astate^\prime \in \reachablestates{\astate_0}{\abstractt}$, $\candidate \not\subseteq \extractcandidate{\astate^\prime}$.
\end{theorem}

\begin{comment}
There is also a special case where the result of the abstract machine execution can be used to decide whether the candidate refers to a feasible deadlock state.
When the CTP is executed in the infinite-buffer setting and the candidate cycle is not induced by message starvation (no up-edges from $\bot$ nodes to an orphaned non-blocking action), the filtered deadlock can be validated by consulting the number of send and receive actions at each candidate endpoint.
Specifically, if the number of issued send actions (excluding wildcard sends) is less than or equal to the number of issued receive actions in the final state reached by $\abstractt$, then the deadlock must be feasible.
A sketch of the proof for this fact also appears in supplementary material.
\end{comment}

